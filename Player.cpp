#include "Arduino.h"
#include "Player.h"

Player::Player(int playerNumber, uint32_t playerColor, long startingPosition) {
    number = playerNumber;
    color = playerColor;
    normalColor = color;
    position = startingPosition * 10000;
    lastPosition = position;

    velocity = 0;
    maxVelocity = 500;
    //maxVelocity = 50;

    acceleration = 0;
}

void Player::update(unsigned long deltaTime) {
    //let this player control it self for now
    if (position >= 230000) {
        position = 229999;
        velocity = 0;
        color = normalColor;
        acceleration = -1;
    }

    if (position < 0) {
        position = 0;
        velocity = 0;
        color = normalColor;
        acceleration = 1;
    }
    //Serial.println(deltaTime, DEC);
    //Serial.println((acceleration * deltaTime) / 2, DEC);

    velocity += (acceleration * deltaTime);
    if (velocity > maxVelocity) {
        velocity = maxVelocity;
        //color = 255;
    } else if (velocity < -maxVelocity) {
        velocity = -maxVelocity;
        //color = 255;
    }

    lastPosition = position;
    position += velocity * deltaTime;
}

void Player::collided(Player* collidedWith) {
    velocity = 0;
    acceleration = 0;
}
