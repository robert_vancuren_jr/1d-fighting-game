class Player {
    public:
    Player(int, uint32_t, long);
    void update(unsigned long deltaTime);
    void collided(Player* collidedWith);
    int number;
    long position;
    long lastPosition;
    uint32_t color;
    uint32_t normalColor;

    int velocity;
    int maxVelocity;
    int acceleration;

};
