#include "Adafruit_NeoPixel.h"
#include "Player.h"
#include "HumanController.h"

#define LED_DATA_PIN 7
#define NUMBER_OF_LEDS 23

Adafruit_NeoPixel strip = Adafruit_NeoPixel(
    NUMBER_OF_LEDS,
    LED_DATA_PIN,
    NEO_GRB + NEO_KHZ800);

Player player1 = Player(222, strip.Color(255, 0, 0), 0);
HumanController player1Controller(&player1, 2, 4, 0, 0, 0);

Player player2 = Player(145, strip.Color(255, 255, 0), 23);


Player *entities[] = {&player1, &player2};

unsigned long currentTime = 0;
unsigned long lastTime = 0;
unsigned long deltaTime = 0;

void setup() {
    Serial.begin(9600);
    Serial.println("1D fighter");

    //Todo figure out max power draw and set accordingly
    strip.setBrightness(80);

    //testPositionMapping();
    //testLEDMapping();

    entities[0]->acceleration = 1;
    entities[1]->acceleration = -1;

    //TODO used for debugging collision
    //entities[1]->position -= 10;

    initGame();
}

void initGame() {
    printPlayerInfo(player1);
    //player1Controller.print();

    strip.begin();
    strip.show();

    currentTime = millis();
    lastTime = currentTime;
}

void testPositionMapping() {
    Serial.println("Testing Position mapping");
    for (int i = 0; i < 200; i++) {
        Serial.print("mapping pos ");
        Serial.print(i, DEC);
        Serial.print(" to ");
        Serial.print(mapPositionToLED(i), DEC);
        Serial.println("");
    }
}

void testLEDMapping() {
    Serial.println("Testing LED mapping");
    for (int i = 0; i < 10; i++) {
        Serial.print("mapping LED ");
        Serial.print(i, DEC);
        Serial.print(" to ");
        Serial.print(mapLEDToPosition(i), DEC);
        Serial.println("");
    }
}

long mapPositionToLED(long position) {
    return position / 10000;
}

long mapLEDToPosition(long led) {
    return led * 10000;
}

void printPlayerInfo(Player player) {
    Serial.println("------------");
    Serial.print("Player: ");
    Serial.print(player.number);
    Serial.print(" position ");
    Serial.print(player.position);

    Serial.println("");
}

void loop() {
    gameLoop();
}

void gameLoop() {
    currentTime = millis();
    deltaTime = currentTime - lastTime;
    lastTime = currentTime;

    //player1Controller.update();

    for (int i = 0; i < 2; i++) {
        Player* entity = entities[i];
        entity->update(deltaTime);
    }

    checkCollisions(deltaTime);

    render();
}

void checkCollisions(unsigned long deltaTime) {
    Player* entity1 = entities[0];
    Player* entity2 = entities[1];
    int entity1Pos = entity1->position / 10000;
    int entity2Pos = entity2->position / 10000;

    if (entity1Pos >= entity2Pos) {
        Serial.println("collide");
        Serial.println(entity1Pos);
        Serial.println(entity2Pos);
        //handle pos equal first
        if (entity1Pos == entity2Pos) {
            int depth1 = entity1->position % 10000;
            int depth2 = 9999 - (entity2->position % 10000);


            //TODO whats fair when they are equal
            //maybe look at velocity and acceleration?
            if (depth1 >= depth2) {
                Serial.println("player one gets led");
                long newPosition = (entity1Pos + 1l) * 10000l;

                entity1->position = newPosition - 1;
                entity2->position = newPosition;
            } else {
                Serial.println("player two gets led");
                long newPosition = (entity1Pos) * 10000l;

                entity1->position = newPosition - 1;
                entity2->position = newPosition;
            }
        } else {
            Serial.println("different led");
            //TODO figure out which player is where
            //based on the last position + velocity and time
            //but we can probably do something simple if we
            //check often enough
            long pos2 = (entity1Pos) * 10000l;
            long pos1 = pos2 - 1;
            entity1->position = pos1;
            entity2->position = pos2;
        }

        entity1->collided(entity2);
        entity2->collided(entity1);

        debugReset();
    } else {
        Serial.println("nope");
    }
}

void debugReset() {
    render(); 
    /*
    delay(500);
    currentTime = millis();
    lastTime = currentTime;
    */

    long position1 = random(0, 10) * 10000l;
    long position2 = random(13, 24) * 10000l;
    Serial.println("reset");
    Serial.println(position1);
    Serial.println(position2);
    Player* entity1 = entities[0];
    Player* entity2 = entities[1];
    entity1->position = position1;
    entity2->position = position2;
    //entity1->position = random(0, 10) * 10000l;
    //entity2->position = random(13, 24) * 10000l;

    entity1->velocity = 0;
    entity2->velocity = 0;

    entity1->acceleration = 1;
    entity2->acceleration = -1;
}

void render() {
    clearStrip(strip.Color(0, 0, 0));

    for (int i = 0; i < 2; i++) {
        Player* entity = entities[i];
        strip.setPixelColor(entity->position / 10000, entity->color);
    }

    strip.show();
}

void clearStrip(uint32_t c) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
  }
}
